<div id="{{ #block }}" class="widget do_adminwidget widget-active">
    <h3 class="widget-header">
    Block
            <input disabled type="text" class="block-name" name="block-{{#s}}-name" id="block-{{#s}}-name" value="{{ tblk|escape }}" title="{_ Block name _}" placeholder="{_ name _}" />
        </h3>
        <div class="widget-content">
        <div class="widget-footer">
          {% include "_admin_edit_block_addblock.tpl" %}
         </div>
    </div>
</div>