{% with m.template.by_id[id] as template %}
{% with blocks|if_undefined:(m.admin_blocks.list[id]) as blocks %}
<div id="edit-blocks-wrapper">
<div id="edit-blocks">
    <input type="hidden" id="block-" name="block-" value="" />
    {% for tblk in template.blocks %}
        {% for blk in id.blocks|filter:`name`:tblk %}
            {% include "_admin_edit_block_li.tpl" %}
         {% empty %}
            {% include "_admin_add_block_li.tpl" %}
        {% endfor %}
    {% endfor %}
</div>
</div>
{% endwith %}
{% endwith %}

{% all include "_admin_edit_blocks_additional.tpl" %}

{% javascript %}

$('#edit-blocks').on('click', '.icon-remove', function(event) {
    event.stopPropagation();
    var block = $(this).closest('div.widget').attr('id');
    var name = $(this).closest('div.widget').find('input.block-name').val();
    z_dialog_confirm({
        title: '{_ Confirm block removal _}',
        text: '<p>{_ Do you want to remove this block? _}</p>',
        cancel: '{_ Cancel _}',
        ok: '{_ Delete _}',
        on_confirm: function() {
                        z_notify('blocks-remove-block', {
                                        z_delegate: 'mod_zm_blocks',
                                        rsc_id: {{ id }},
                                        name: name,
                                        block: block,
                                    });
                    }
    })
});

$('#edit-blocks').on('click', '.block-add-block .dropdown-menu a', function(event) {
    var block_type = $(this).data('block-type');
    var block = $(this).closest('div.widget').attr('id');
    var name = $(this).closest('div.widget').find('input.block-name').val();
    var langs = '';

    $('input[name=language]:checked').each(function() { langs += ',' + $(this).val(); });

    z_notify('blocks-insert-block', {
                z_delegate: 'mod_zm_blocks',
                type: block_type,
                rsc_id: {{ id }},
                name: name,
                block: block,
                language: langs,
                edit_language: $('.language-tabs .active').attr('lang')
            });
    event.preventDefault();
});

$('#edit-blocks').on('click', '.block-page a.page-connect', function(event) {
    window.zBlockConnectTrigger = this;
    z_event("admin-block-connect", {});
    event.preventDefault();
});

window.zAdminBlockConnectDone = function(v) {
    var $block_page = $(window.zBlockConnectTrigger).closest(".block-page");
    var target_id = $(".rsc-item-wrapper", $block_page).attr('id');
    $("input[type=hidden]", $block_page).val(v.object_id);
    z_notify("update", {z_delegate: 'mod_admin', template: "_rsc_item.tpl", id: v.object_id, z_target_id: target_id});
    window.zAdminConnectDone(v);
}

$('#edit-blocks').on('click', '.rsc-item h5 a', function(event) {
    var rsc_id = $(this).attr('href').replace('#', '');
    z_event("admin-edit-basics", {
                        id: rsc_id,
                        element_id: $(this).closest(".rsc-item").attr('id'),
                        template: "_rsc_item.tpl",
                        edit_dispatch: "{{ edit_dispatch }}"
                });
    event.preventDefault();
});

{% endjavascript %}

{% wire name="admin-block-connect"
        action={dialog_open
                    subject_id=id
                    predicate=""
                    template="_action_dialog_connect.tpl"
                    title=_"Find page"
                    callback="window.zAdminBlockConnectDone"}
%}

{% wire name="admin-edit-basics" action={dialog_edit_basics template="_rsc_item.tpl"} %}
