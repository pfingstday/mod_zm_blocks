{% with is_editable|default:id.is_editable as is_editable %}
<div id="{{ #block }}" class="block do_adminwidget widget">
    <h3 class="widget-header">
        <span title="{_ Remove _}" class="icon-remove" style="cursor: pointer;"></span>
        {{ blk.type|make_list|capfirst|replace:"_":" " }} {_ block _}
        <input readonly type="text" class="block-name" name="block-{{#s}}-name" id="block-{{#s}}-name" value="{{ blk.name|escape }}" title="{_ Block name _}" placeholder="{_ name _}" />
    </h3>
         <input type="hidden" class="block-type" name="block-{{#s}}-type" value="{{ blk.type }}" />
        {% include ["blocks/_admin_edit_block_li_",blk.type,".tpl"]|join name=#s blk=blk id=id is_editable=is_editable is_new=is_new %}
    {% if is_new %}
        {% javascript %}
            z_tinymce_init();
        {% endjavascript %}
    {% endif %}
</div>
{% endwith %}

{% if is_new %}
{% javascript %}
    $("#{{ #block }} .widget").effect("highlight");
    z_admin_ensure_block_names();
{% endjavascript %}
{% endif %}
