-module(mod_zm_blocks).
-author("Steffen Hanikel <steffen.hanikel@gmail.com>").

-mod_title("Zeitmacht Blocks").
-mod_description("Show predefined blocks from templates on the edit page.").
-mod_prio(550).
-mod_provides([zm_blocks]).
-mod_depends([base, admin, template_meta]).

-include("zotonic.hrl").

-export([event/2]).

event(#postback_notify{message="blocks-insert-block"}, Context) ->
  Language = case z_context:get_q("language", Context) of
               undefined ->
                 [];
               Ls ->
                 Ls1 = string:tokens(Ls, ","),
                 [ list_to_atom(L) || L <- lists:filter(fun z_trans:is_language/1, Ls1) ]
             end,
  EditLanguage = case z_context:get_q("edit_language", Context) of
                   undefined ->
                     z_context:language(Context);
                   EL ->
                     case z_trans:is_language(EL) of
                       true -> list_to_atom(EL);
                       false -> z_context:language(Context)
                     end
                 end,
  Type = z_string:to_name(z_context:get_q("type", Context)),
  Name = z_string:to_name(z_context:get_q("name", Context)),
  RscId = list_to_integer(z_context:get_q("rsc_id", Context)),
  Render = #render{
    template="_admin_edit_block_li.tpl",
    vars=[
      {id, RscId},
      {r_language, Language},
      {edit_language, EditLanguage},
      is_new,
      {is_editable, z_acl:rsc_editable(RscId, Context)},
      {blk, [{type, Type}, {name, Name}]},
      {blocks, lists:sort(z_notifier:foldl(#admin_edit_blocks{id=RscId}, [], Context))}
    ]
  },
  case z_html:escape(z_context:get_q("block", Context)) of
    undefined -> z_render:appear_top("edit-blocks", Render, Context);
    BlockId -> z_render:appear_replace(BlockId, Render, Context)
  end;

event(#postback_notify{message="blocks-remove-block"}, Context) ->
  ?DEBUG(z_context:get_q_all(Context)),
  RscId = z_convert:to_integer(z_context:get_q("rsc_id", Context)),
  Name = z_string:to_name(z_context:get_q("name", Context)),
  Render = #render{
    template="_admin_add_block_li.tpl",
    vars=[
      {id, RscId},
      is_new,
      {is_editable, z_acl:rsc_editable(RscId, Context)},
      {tblk, Name},
      {blocks, lists:sort(z_notifier:foldl(#admin_edit_blocks{id=RscId}, [], Context))}
    ]
  },
  case z_html:escape(z_context:get_q("block", Context)) of
    undefined -> z_render:appear_top("edit-blocks", Render, Context);
    BlockId -> z_render:appear_replace(BlockId, Render, Context)
  end.